<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Modules;

use Contao\Database;
use Contao\Files;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\Module;
use Contao\PageModel;
use Contao\Session;
use Srhinow\SchwarmalarmBundle\Libs\OsmGeoData;
use Srhinow\SchwarmalarmBundle\Libs\OsmStaticMaps;

/**
 * Class ModuleBn
 */
abstract class ModuleSchwarmalarm extends Module
{

	/**
	 * Parse an item and return it as string
	 * @param object
	 * @param string
	 * @param integer
	 * @return string
	 */
	protected function parseBeekeeper($objBk, $strClass='', $intCount=0)
	{
		global $objPage;

		$objTemplate = new FrontendTemplate($this->beekeeper_item_template);
		$objTemplate->setData($objBk->row());
        $objTemplate->showDistance = $this->showDistance ;
		$objTemplate->class = (($objBk->cssClass != '') ? ' ' . $objBk->cssClass : '') . $strClass;

		//Detail-Url
		if($this->jumpTo)
		{
			$objDetailPage = PageModel::findByPk($this->jumpTo);

			$objTemplate->detailUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row(),'/lib/'.$objBk->id) );
		}

		return $objTemplate->parse();
	}


	/**
	 * Parse one or more items and return them as array
	 * @param object
	 * @return array
	 */
	protected function parseBeekeepers($objBks)
	{
		$limit = $objBks->count();

		if ($limit < 1)
		{
			return array();
		}

		$count = 0;
		$arrBks = array();

		while ($objBks->next())
		{
            $objBks->distance = $this->getDistance($objBks);
            $arrBks[$objBks->distance][] = array
            (
                'data' => $objBks->row(),
                'html' => $this->parseBeekeeper($objBks, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count)
            );

		}

		ksort($arrBks);
		return $arrBks;
	}


	/**
	* return lat and lon from given location in the session
	* @param string
	* @return array
	*/
	public function getGeoDataFromCurrentPosition($searchVal='')
	{
		$session = Session::getInstance()->get($GLOBALS['SCHWARMALARM']['SESSION_KEY']);
		$plzcity = (strlen($searchVal)>0)?$searchVal : $session['plzcity'];
		$plzcity = trim($plzcity);
		$geodata = array();

		if(strlen($plzcity) < 1) return $geodata;

		$geo = new OsmGeoData();

		$searchData = $this->splitAdressString($plzcity);

    	$json = $geo->getGeoData($searchData);

	    if(is_object($json[0]))
	    {
            $geodata['lat'] = $json[0]->lat;
            $geodata['lon'] = $json[0]->lon;
            $geodata['plzcity'] = $searchData['string'];
	    }

	    return $geodata;
	}

    /**
     * @param string $address
     * @return array
     */
	public function splitAdressString($searchStr='') {

	    $return = ['plz' => '', 'address' => '', 'string' => $searchStr];

	    if(strlen($searchStr) < 1) return $return;
	    $parts = explode(' ', $searchStr,2);

	    if(is_numeric($parts[0])) {
            $plz = $parts[0];
            $address = $parts[1];
        } else {
            $address = implode(' ',$parts);
        }

	    // plz pruefen und schreiben
        if(isset($plz) && strlen((int) $plz) === 5) $return['plz'] = $plz;

        // adresse bereinigen und schreiben
        $address = trim( strip_tags( str_replace(',','',$address)));
        $return['address'] = $address;

        // suchstring bereinigen und schreiben
        $return['string'] = trim( strip_tags( $return['string']));

        return $return;
    }

	/**
	* get the distance from search-location and library
	* @param object
	* @return string
	*/
	public function getDistance($objBeekeeper)
	{
		$session = Session::getInstance()->get($GLOBALS['SCHWARMALARM']['SESSION_KEY']);
//		print_r((int) $session['distance']); die();
		$distance = '';

		if(strlen($session['geo_lat']) > 0 && strlen($session['geo_lon']) > 0 && strlen($session['distance']) > 0)
		{
			$resultObj = Database::getInstance()->prepare("SELECT 
				ACOS(SIN(RADIANS(lat)) * SIN(RADIANS(?)) + COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lon)- RADIANS(?))) * 6380 AS `distance` 
				FROM `tl_schwarmalarm_beekeeper`
				WHERE `tl_schwarmalarm_beekeeper`.`id` = ?
				AND ACOS( SIN(RADIANS(lat)) * SIN(RADIANS(?)) + COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lon) - RADIANS(?))) * 6380 <= ?")
			->limit(1)
			->execute(
					$session['geo_lat'], $session['geo_lat'], $session['geo_lon'],
					$objBeekeeper->id,
					$session['geo_lat'], $session['geo_lat'], $session['geo_lon'],$session['distance']
				);

			if($resultObj->numRows > 0) $distance = $resultObj->distance;
		}

		return $distance;
	}


    /**
     * @param string $dirPath
     * @return bool
     */
    public static function checkOrCreateDir($dirPath='') {

	    if(strlen($dirPath) < 1) return false;

	    // ToDo try-catch
        if (!is_dir($dirPath) )
        {
            Files::getInstance()->mkdir($dirPath);
        }

        return (is_dir($dirPath))? true : false;
    }

    /**
     * pruefen ob gerade Schwarmzeit ist
     * @return bool
     */
    public function isSwarmTime() {
        $isSaison = true;

        $start = unserialize($this->recurring_start_date);
        $end = unserialize($this->recurring_end_date);

        if(strlen($start[0]['recurring_month']) > 0 && strlen($end[0]['recurring_month']) > 0) {
            $saison = [
                'start'=>mktime(0,0,0,(int)$start[0]['recurring_month'],(int)$start[0]['recurring_day'],date('Y')),
                'end'=>mktime(0,0,0,(int)$end[0]['recurring_month'],(int)$end[0]['recurring_day'],date('Y'))
            ];
            $isSaison = (time() >= $saison['start'] && time() <= $saison['end']) ? true : false;
        }

        return $isSaison;
    }

}
