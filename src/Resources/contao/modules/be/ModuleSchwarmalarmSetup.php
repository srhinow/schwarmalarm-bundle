<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Modules;

use Contao\BackendModule;

/**
 * Class ModuleBNSetup
 * Back end module Isotope "setup".
 */
class ModuleSchwarmalarmSetup extends BackendModule
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_properties';


    /**
     * Change the palette of the current table and switch to edit mode
     */
    public function generate()
    {
        return $this->objDc->edit($GLOBALS['SCHWARMALARM']['SETUP']['ID']);
    }

    /**
     * Generate module
     */
    protected function compile()
    {
        return '';
    }
}

