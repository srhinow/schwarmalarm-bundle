<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Modules;

use Contao\BackendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Session;
use Srhinow\SchwarmalarmBundle\Models\SchwarmalarmBeekeeperModel;


/**
 * Class ModuleBnSearchRegion
 */
class ModuleBeekeeperSearchForm extends ModuleSchwarmalarm
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_beekeeper_search_form';

	protected $strFormSubmit = 'beekeeper_search_form';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### IMKER-REGION-SUCHE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Ajax Requests abfangen
		if(Input::get('id') && $this->Environment->get('isAjaxRequest')){
		     $this->generateAjax();
		     exit;
		}

        // template overwrite
		if (strlen($this->mod_bn_template)) $this->strTemplate = $this->mod_bn_template;

		// Set the item from the auto_item parameter
		if (!isset($_GET['s']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('s', Input::get('auto_item'));
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
		$this->import('FrontendUser','User');

		$this->Template->session = false;
		$session = Session::getInstance()->get($GLOBALS['SCHWARMALARM']['SESSION_KEY'])?: array();

		//Formular verarbeiten wenn es gesendet wurde
		if(Input::post('FORM_SUBMIT') == $this->strFormSubmit)
		{
			if(Input::post('filter_reset') == 1)
			{
				$session = array();

			}
			else
			{
				$geodata = $this->getGeoDataFromCurrentPosition(Input::post('plzcity'));

				$session = array
				(
					'plzcity' => $geodata['plzcity'],
					'distance' => Input::post('distance'),
					'only_open' => Input::post('only_open'),
					'leistungen' => Input::post('leistungen'),
					'medien' => Input::post('medien'),
					'geo_lat' => $geodata['lat'],
					'geo_lon' => $geodata['lon']
				);
				
			}

			Session::getInstance()->set($GLOBALS['SCHWARMALARM']['SESSION_KEY'], $session);
			Input::setPost('FORM_SUBMIT','');

			if($this->jumpTo)
			{
				$objDetailPage = PageModel::findByPk($this->jumpTo);
				$listUrl = ampersand( \Controller::generateFrontendUrl($objDetailPage->row()) );
				$this->redirect($listUrl);
			} else {
                global $objPage;
                $objPage->noSearch = 1;
                $objPage->cache = 0;

                $objTarget = PageModel::findByPk($objPage->id);
                if ($objTarget !== null) {
                    $reloadUrl = ampersand(\Controller::generateFrontendUrl($objTarget->row()));
                }

                $this->redirect($reloadUrl);
            }
//			$this->reload();
		}

		if(strlen($session['geo_lat'])>0 && strlen($session['geo_lon'])>0)
		{
			$geodata = array
			(
				'lat'=>$session['geo_lat'],
				'lon'=>$session['geo_lon']
			);
		}
		else
		{
			$geodata = $this->getGeoDataFromCurrentPosition();
		}


		// Get the total number of items
		$intTotal = SchwarmalarmBeekeeperModel::countBeekeepersEntries($geodata,$session['distance']);

		// Filter anwenden um die Gesamtanzahl zuermitteln
		if($intTotal > 0)
		{
			$filterLibsObj = SchwarmalarmBeekeeperModel::findBeekeepers($intTotal, 0, $geodata, $session['distance']);

			$counter = 0;
			$idArr = array();

			while($filterLibsObj->next())
			{
				//wenn alle Filter stimmen -> Werte setzen
				$idArr[] = $filterLibsObj->id;
				$counter++;
			}
			if((int) $intTotal > $counter) $intTotal = $counter;
		}
		$this->Template->total = (int) $intTotal;

		if(count($session) > 0)
		{
			$this->Template->session = true;
			$this->Template->plzcity = $session['plzcity'];
			$this->Template->distance = $session['distance'];
		}
		else
		{
			//default-Werte setzen
			$this->Template->distance = 15;
		}

        $this->Template->strFormSubmit = $this->strFormSubmit;
	}

	public function generateAjax()
	{
//        $this->getAutocompleteFromOsm();
        $this->getAutocompleteFromDb();
	}

    /**
     * holt die Autocomplete-Einträge von nominatim.openstreetmap.org
     * gibt per Ajax-Response zurueck
     */

    public function getAutocompleteFromOsm() {

	    $plzort = trim($_GET['term']);

        if((strlen($plzort) >= 2) && (strlen($plzort) <= 10))
        {
            $addressStr = urlencode($plzort);
//        print_r(strrpos($dc->activeRecord->street, " "));
            $searchData = [
                'string' => $addressStr
            ];
//             print_r($searchData);
            $geo = new \Srhinow\SchwarmalarmBundle\Libs\OsmGeoData();

            $json = $geo->getGeoData($searchData);
//            print_r($json);  exit();

//            if(is_object($json))
//            {
//                while($resObj->next())
//                {
//                    $items[] = $resObj->zc_zip .' '. $resObj->zc_location_name;
//                }
//                $items[] = $_GET['term'];
//                header('Content-type: application/json');
//                echo json_encode($items);
//                exit();
//            }
        }

    }

    /**
     * holt die Autocomplete-Einträge aus der Datenbank
     * gibt per Ajax-Response zurueck
     */
	public function getAutocompleteFromDb() {
        $plzort = trim($_GET['term']);

        if((strlen($plzort) >= 2) && (strlen($plzort) <= 10))
        {
            //prüfen ob es eine Zahl ist (plz)
            if (is_numeric($plzort))
            {
                $resObj = $this->Database->prepare("SELECT `zc_zip`, `zc_location_name` FROM `tl_zip_coordinates` WHERE `zc_zip` LIKE ?")
                    ->limit(50)
                    ->execute($plzort.'%');
            }
            else{
                $resObj = $this->Database->prepare("SELECT `zc_zip`, `zc_location_name` FROM `tl_zip_coordinates` WHERE `zc_location_name` LIKE ?")
                    ->limit(50)
                    ->execute($plzort.'%');

            }

            if ($resObj->numRows > 0)
            {
                while($resObj->next())
                {
                    $items[] = $resObj->zc_zip .' '. $resObj->zc_location_name;
                }
                $items[] = $_GET['term'];
                header('Content-type: application/json');
                echo json_encode($items);
                exit();
            }
        }

    }

}
