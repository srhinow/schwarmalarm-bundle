<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Modules;


use Contao\BackendTemplate;
use Contao\Controller;
use Contao\Database;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Pagination;
use mysql_xdevapi\Session;
use Srhinow\SchwarmalarmBundle\Models\SchwarmalarmBeekeeperModel;


/**
 * Class ModuleBeekeeperSearchList
 */
class ModuleVerifyOptOutLink extends ModuleSchwarmalarm
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_beekeeper_message';

    /**
     * @var bool
     */
    public $showDistance = true;

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### IMKER-VERIFY-OPTOUT-LINK ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     */
    protected function compile()
    {
        $key = Input::get('key');

        // fehlender key
        if(strlen($key) < 1) {
            $this->Template->message = $this->message_false_url;
            return true;
        }

        $ObjBeekeeper = SchwarmalarmBeekeeperModel::findBy('key',$key);
        
        // keinen passenden Eintrag gefunden
        if(null === $ObjBeekeeper) {
            $this->Template->message = $this->message_no_entry;
            return true;
        }

        // wenn der Eintrag bereist verifiziert wurde
        if($ObjBeekeeper->verified == '1') {
            $this->Template->message = $this->message_allready_verified;
            return true;
        }

        $ObjBeekeeper->verified = '1';
        $ObjBeekeeper->save();

        if($this->jumpTo)
        {
            $objDetailPage = PageModel::findByPk($this->jumpTo);
            $listUrl = ampersand( \Controller::generateFrontendUrl($objDetailPage->row()) );
            $this->redirect($listUrl);
        }
    }
}
