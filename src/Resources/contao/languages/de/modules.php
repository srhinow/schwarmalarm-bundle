<?php

/**
 * TL_ROOT/vendor/srhinow/schwarmalarm-bundle/src/Recources/contao/modules/languages/de/modules.php
 *
 * Contao extension: themecontent-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['schwarmalarm'] = array('Schwarmalarm', '');
$GLOBALS['TL_LANG']['MOD']['tl_schwarmalarm_beekeeper'] = array('Imker', '');
$GLOBALS['TL_LANG']['MOD']['tl_schwarmalarm_properties'] = array('Einstellungen', '');
$GLOBALS['TL_LANG']['MOD']['tl_zip_coordinates'] = array('PLZ-Koordinaten', '');
