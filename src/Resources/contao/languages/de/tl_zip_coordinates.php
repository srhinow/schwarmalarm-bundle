<?php
/**
 * TL_ROOT/vendor/srhinow/schwarmalarm-bundle/src/Recources/contao/modules/languages/de/tl_schwarmalarm_beekeeper.php
 *
 * Contao extension: schwarmalarm-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/** Legends */
$GLOBALS['TL_LANG']['tl_zip_coordinates']['address_legend'] = "Adressdaten";
$GLOBALS['TL_LANG']['tl_zip_coordinates']['geo_legend'] = "Geo Einstellungen";
$GLOBALS['TL_LANG']['tl_zip_coordinates']['extend_legend'] = "weitere Einstellungen";

/** Legends */
$GLOBALS['TL_LANG']['tl_zip_coordinates']['dateAdded'] = ["hinzugefügt am"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_zip'] = ["PLZ"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_location_name'] = ["Ort"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_county'] = ["Landkreis"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_locality'] = ["Ortschaft"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_state'] = ["Bundesland"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_lat'] = ["GEO-Latitude"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_lon'] = ["GEO-Longitude"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['updated'] = ["update"];
$GLOBALS['TL_LANG']['tl_zip_coordinates']['dateUpdated'] = ["Update-Datum"];
