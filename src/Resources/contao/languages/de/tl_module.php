<?php

$GLOBALS['TL_LANG']['tl_module']['mod_schwarmalaerm_template'] = ['Modul-Template'];
$GLOBALS['TL_LANG']['tl_module']['item_template'] = ['List-Eintrag-Template'];
$GLOBALS['TL_LANG']['tl_module']['recurring_start_date'] = ['Start der Schwarmzeit'];
$GLOBALS['TL_LANG']['tl_module']['recurring_end_date'] = ['Ende der Schwarmzeit'];
$GLOBALS['TL_LANG']['tl_module']['recurring_day'] = ['Tag'];
$GLOBALS['TL_LANG']['tl_module']['recurring_month'] = ['Monat'];
$GLOBALS['TL_LANG']['tl_module']['message_false_url'] = ['Text :: falsche URL','(fehlender key-Paramter)'];
$GLOBALS['TL_LANG']['tl_module']['message_no_entry'] = ['Text :: kein Eintrag in der Datenbank gefunden',''];
$GLOBALS['TL_LANG']['tl_module']['message_allready_verified'] = ['Text :: der Eintrag wurde bereits verifiziert.',''];

$GLOBALS['TL_LANG']['tl_module']['schwarmzeit_legend'] = 'Schwarmzeit-Einstellungen';