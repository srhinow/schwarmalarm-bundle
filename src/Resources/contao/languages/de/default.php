<?php

/**
 * TL_ROOT/vendor/srhinow/schwarmalarm-bundle/src/Recources/contao/modules/languages/de/modules.php
 *
 * Contao extension: themecontent-bundle
 * German translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0-or-later
 * Translator: Fritz Michael Gschwantner <https://github.com/fritzmg>
 */

$GLOBALS['TL_LANG']['CTE']['schwarmalarm'] = ['Schwarmalarm', ''];
