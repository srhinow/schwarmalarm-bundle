<?php
/**
 * Created by schwarmalarm.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 29.12.18
 */

/**
 * Table tl_zip_coordinates
 */
$GLOBALS['TL_DCA']['tl_zip_coordinates'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'switchToEdit'                => true,
        'enableVersioning'            => true,
        'onload_callback' => array
        (
//            array('tl_zip_coordinates', 'checkPermission')
        ),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'zc_id' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('dateAdded DESC'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('zc_zip', 'zc_location_name', 'zc_county', 'zc_locality','zc_lat','zc_lon'),
            'showColumns'             => true,
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.svg',
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'cut' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_page']['cut'],
                'href'                => 'act=paste&amp;mode=cut',
                'icon'                => 'cut.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset()"',
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('protected'),
        'default'                     => '{address_legend},zc_zip,zc_location_name,zc_county,zc_locality,zc_state;{geo_legend:hide},zc_lat,zc_lon;{extend_legend:hide},updated,dateUpdated',
    ),
    // Subpalettes
    'subpalettes' => array
    (
        'protected'                   => 'groups',
    ),
    // Fields
    'fields' => array
    (
        'id' => array
        (
            'label'                   => array('ID'),
            'search'                  => true,
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'zc_id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'zc_loc_id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'dateAdded' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['MSC']['dateAdded'],
            'default'                 => time(),
            'sorting'                 => true,
            'flag'                    => 6,
            'eval'                    => array('rgxp'=>'datim', 'doNotCopy'=>true),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'zc_zip' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_zip'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>5, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'zc_location_name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_location_name'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'zc_county' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_county'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>55, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(55) NOT NULL default ''"
        ),
        'zc_locality' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_locality'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'zc_state' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_state'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'zc_lat' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_lat'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "float(2,10) NOT NULL default 0"
        ),
        'zc_lon' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['zc_lon'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "float(2,10) NOT NULL default 0"
        ),
        'updated' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['updated'],
            'exclude'                 => true,
            'default'                 => 1,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'dateUpdated' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_zip_coordinates']['dateUpdated'],
            'default'                 => time(),
            'sorting'                 => true,
            'flag'                    => 6,
            'eval'                    => array('rgxp'=>'datim', 'doNotCopy'=>true),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        )
    )
);
