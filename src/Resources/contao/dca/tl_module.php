<?php
/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2014 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    bn_libraries (www.bibliotheken-niedersachsen.de/)
 * @license    commercial
 * @filesource
 */

/**
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['beekeeper_search_form'] = '{title_legend},name,headline,type;{config_legend},jumpTo;{template_legend:hide},mod_schwarmalaerm_template;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['beekeeper_search_list'] = '{title_legend},name,headline,type;{config_legend},jumpTo,numberOfItems,perPage;{template_legend:hide},beekeeper_item_template,mod_schwarmalaerm_template;{schwarmzeit_legend},recurring_start_date,recurring_end_date;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['beekeeper_search_map'] = '{title_legend},name,headline,type;{config_legend},jumpTo;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['beekeeper_edit_entry'] = '{title_legend},name,headline,type;{config_legend},jumpTo;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['beekeeper_verify_link'] = '{title_legend},name,headline,type;{config_legend},message_false_url,message_no_entry,message_allready_verified,jumpTo';

$GLOBALS['TL_DCA']['tl_module']['fields']['beekeeper_item_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['item_template'],
	'default'                 => 'item_list_html',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_schwarmalarm', 'getItemTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['mod_schwarmalaerm_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['mod_schwarmalaerm_template'],
	'default'                 => 'mod_bn_search_form',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_schwarmalarm', 'getModulTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['recurring_start_date'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_start_date'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'columnFields' => [
            'recurring_day'      => [
                'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_day'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'style'              => 'width:180px',
                    'includeBlankOption' => true,
                ],
                'options_callback'   => ['tl_module_schwarmalarm','generateDayOptions'],
            ],
            'recurring_month' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_month'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'style'              => 'width:180px',
                    'includeBlankOption' => true,
                ],
                'options_callback'   => ['tl_module_schwarmalarm','generateMonthOptions'],
            ],
        ],
        'multiple' => false,
        'maxCount' => 1
    ],
    'sql'       => 'blob NULL',
];


$GLOBALS['TL_DCA']['tl_module']['fields']['recurring_end_date'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_end_date'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'columnFields' => [
            'recurring_day'      => [
                'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_day'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'style'              => 'width:180px',
                    'includeBlankOption' => true,
                ],
                'options_callback'   => ['tl_module_schwarmalarm','generateDayOptions'],
            ],
            'recurring_month' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_module']['recurring_month'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'style'              => 'width:180px',
                    'includeBlankOption' => true,
                ],
                'options_callback'   => ['tl_module_schwarmalarm','generateMonthOptions'],
            ],
        ],
        'multiple' => false,
        'maxCount' => 1
    ],
    'sql'       => 'blob NULL',
];
$GLOBALS['TL_DCA']['tl_module']['fields']['message_false_url'] = [

    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['message_false_url'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'search'                  => true,
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
    'sql'                     => "text NULL"
];
$GLOBALS['TL_DCA']['tl_module']['fields']['message_no_entry'] = [

    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['message_no_entry'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'search'                  => true,
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
    'sql'                     => "text NULL"
];
$GLOBALS['TL_DCA']['tl_module']['fields']['message_allready_verified'] = [

    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['message_allready_verified'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'search'                  => true,
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
    'sql'                     => "text NULL"
];

/**
 * Class tl_module_schwarmalarm
 */
class tl_module_schwarmalarm extends Backend
{
	/**
	 * Return all news templates as array
	 * @return array
	 */
	public function getItemTemplates()
	{
		return $this->getTemplateGroup('item_');
	}
	
	/**
	 * Return all news templates as array
	 * @return array
	 */
	public function getModulTemplates()
	{
		return $this->getTemplateGroup('mod_beekeeper_');
	}

	public function generateDayOptions(){
	    $options = [];

	    for($i=1; $i<= 31; $i++) $options[$i] = $i;

	    return $options;
    }

    public function generateMonthOptions(){
        $options = [];

        for($i=1; $i<= 12; $i++) $options[$i] = $i;

	    return $options;
    }

}
