<?php

/**
 *
 * @copyright  Sven Rhinow 2018
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    schwarmalarm-bundle
 * @license    LGPL
 */


/**
 * Table tl_schwarmalarm_properties
 */
$GLOBALS['TL_DCA']['tl_schwarmalarm_properties'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => false,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
        'onload_callback' => array
        (
            array('tl_schwarmalarm_properties', 'create_property_entry')
        ),


    ),
    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 1,
//            'flag'                    => 12,
            'panelLayout'             => 'filter;search,limit'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['edit'],
                'href'                => 'table=tl_schwarmalarm_properties&act=edit',
                'icon'                => 'edit.gif',
                'attributes'          => 'class="contextmenu"'
            ),
            'editheader' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['editheader'],
                'href'                => 'act=edit',
                'icon'                => 'header.gif',
                'attributes'          => 'class="edit-header"'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),

            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
            )

        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{api_settings},api_url,api_key;{new_member_info_legend:hide},newuser_info_email,newuser_info_name,newuser_info_subject',

    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'modify' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'newuser_info_email' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['newuser_info_email'],
            'exclude'                 => true,
            'search'                  => true,
            'filter'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'clr w50'),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'newuser_info_name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['newuser_info_name'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 11,
            'inputType'               => 'text',
            'eval'                    => array('decodeEntities'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'newuser_info_subject' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['newuser_info_subject'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 11,
            'inputType'               => 'text',
            'default'		      => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['newmember_subject_default'],
            'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long'),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'api_url' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['api_url'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr full'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'api_key' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_properties']['api_key'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr full'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
    )
);


/**
 * Class tl_schwarmalarm_properties
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 */
class tl_schwarmalarm_properties extends \Backend
{

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * create an entry if id=1 not exists
     * @return none
     */
    public function create_property_entry()
    {
        $testObj = $this->Database->execute('SELECT * FROM `tl_schwarmalarm_properties`');

        if($testObj->numRows == 0)
        {
            $this->Database
                ->prepare('INSERT INTO `tl_schwarmalarm_properties`(`id`) VALUES(?)')
                ->execute($GLOBALS['SCHWARMALARM']['SETUP']['ID']);
        }
    }

}

