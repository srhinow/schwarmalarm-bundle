<?php

/**
 *
 * @copyright  Sven Rhinow 2018
 * @author     Sven Rhinow <sven@sr-tag.de>
 * @package    schwarmalarm-bundle
 * @license    LGPL
 */

/**
 * Load class tl_page
 */
$this->loadDataContainer('tl_page');


/**
 * Table tl_schwarmalarm_beekeeper
 */
$GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'switchToEdit'                => true,
        'enableVersioning'            => true,
        'onsubmit_callback' => array
        (
            array('tl_schwarmalarm_beekeeper', 'setFirstGeoLatLon')
            // array('tl_bn_libraries', 'setAllGeoLatLon')
        ),
        'onload_callback' => array
        (
//            array('tl_schwarmalarm_beekeeper', 'checkPermission')
        ),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'zc_id,published' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('dateAdded DESC'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('name','zipcode', 'country', 'street', 'dateAdded'),
            'showColumns'             => true,
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.svg',
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'cut' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_page']['cut'],
                'href'                => 'act=paste&amp;mode=cut',
                'icon'                => 'cut.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset()"',
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['toggle'],
                'icon'                => 'visible.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_schwarmalarm_beekeeper', 'toggleIcon'),
                'showInHeader'        => true
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('protected'),
        'default'                     => '{address_legend},name,phone,zipcity,zipcode,country,street,additional;{geo_legend:hide},lat,lon,setnewgeo;{extend_legend:hide},key,published',
    ),
    // Subpalettes
    'subpalettes' => array
    (
        'protected'                   => 'groups',
    ),
    // Fields
    'fields' => array
    (
        'id' => array
        (
            'label'                   => array('ID'),
            'search'                  => true,
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'zc_id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'dateAdded' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['MSC']['dateAdded'],
            'default'                 => time(),
            'sorting'                 => true,
            'flag'                    => 6,
            'eval'                    => array('rgxp'=>'datim', 'doNotCopy'=>true),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['name'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>125, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(125) NOT NULL default ''"
        ),
        'phone' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['phone'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>55, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(55) NOT NULL default ''"
        ),
        'zipcity' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcity'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>125, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(125) NOT NULL default ''"
        ),
        'zipcode' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcode'],
            'exclude'                 => true,
            'search'                  => true,
            'filter'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>5, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(5) NOT NULL default ''"
        ),
        'country' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['country'],
            'exclude'                 => true,
            'search'                  => true,
            'filter'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>55, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(55) NOT NULL default ''"
        ),
        'street' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['street'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>55, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(55) NOT NULL default ''"
        ),
        'email' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['email'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>55, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(55) NOT NULL default ''"
        ),
        'additional' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['additional'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'key' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['key'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'lat' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lat'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "float(2,10) NOT NULL default 0"
        ),
        'lon' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lon'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "float(2,10) NOT NULL default 0"
        ),
        'setnewgeo' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_bn_libraries']['setnewgeo'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'sql'                     => "char(1) NOT NULL default ''",
            'eval'                    => array('tl_class'=>'clr'),
            'save_callback' => array
            (
                array('tl_schwarmalarm_beekeeper', 'setNewGeoLatLon')
            ),
        ),
        'published' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['published'],
            'exclude'                 => true,
            'filter'                  => true,
            'default'                 => 1,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'verified' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['verified'],
            'exclude'                 => true,
            'filter'                  => true,
            'default'                 => '',
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        )
    )
);

class tl_schwarmalarm_beekeeper extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Check permissions to edit table tl_schwarmalarm_beekeeper
     *
     * @throws Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission()
    {
        if ($this->User->isAdmin)
        {
            return;
        }

        // Set root IDs
        if (!is_array($this->User->schwarmalarms) || empty($this->User->schwarmalarms))
        {
            $root = array(0);
        }
        else
        {
            $root = $this->User->schwarmalarms;
        }

        $GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['list']['sorting']['root'] = $root;

        // Check permissions to add archives
        if (!$this->User->hasAccess('create', 'schwarmalarmp'))
        {
            $GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['config']['closed'] = true;
        }

        /** @var Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Check current action
        switch (Input::get('act') && Input::get('act') != 'paste')
        {
            case 'create':
            case 'select':
                // Allow
                break;

            case 'edit':
                // Dynamically add the record to the user profile
                if (!in_array(Input::get('id'), $root))
                {
                    /** @var Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface $objSessionBag */
                    $objSessionBag = $objSession->getBag('contao_backend');

                    $arrNew = $objSessionBag->get('new_records');

                    if (is_array($arrNew['tl_schwarmalarm_beekeeper']) && in_array(Input::get('id'), $arrNew['tl_schwarmalarm_beekeeper']))
                    {
                        // Add the permissions on group level
                        if ($this->User->inherit != 'custom')
                        {
                            $objGroup = $this->Database->execute("SELECT id, schwarmalarms, schwarmalarmp FROM tl_user_group WHERE id IN(" . implode(',', array_map('intval', $this->User->groups)) . ")");

                            while ($objGroup->next())
                            {
                                $arrTeasermanagerp = StringUtil::deserialize($objGroup->newp);

                                if (is_array($arrTeasermanagerp) && in_array('create', $arrTeasermanagerp))
                                {
                                    $arrTeasermanagers = StringUtil::deserialize($objGroup->news, true);
                                    $arrTeasermanagers[] = Input::get('id');

                                    $this->Database->prepare("UPDATE tl_user_group SET schwarmalarms=? WHERE id=?")
                                        ->execute(serialize($arrTeasermanagers), $objGroup->id);
                                }
                            }
                        }

                        // Add the permissions on user level
                        if ($this->User->inherit != 'group')
                        {
                            $objUser = $this->Database->prepare("SELECT schwarmalarms, schwarmalarmp FROM tl_user WHERE id=?")
                                ->limit(1)
                                ->execute($this->User->id);

                            $arrTeasermanagerp = StringUtil::deserialize($objUser->schwarmalarmp);

                            if (is_array($arrTeasermanagerp) && in_array('create', $arrTeasermanagerp))
                            {
                                $arrTeasermanagers = StringUtil::deserialize($objUser->schwarmalarms, true);
                                $arrTeasermanagers[] = Input::get('id');

                                $this->Database->prepare("UPDATE tl_user SET schwarmalarms=? WHERE id=?")
                                    ->execute(serialize($arrTeasermanagers), $this->User->id);
                            }
                        }

                        // Add the new element to the user object
                        $root[] = Input::get('id');
                        $this->User->schwarmalarms = $root;
                    }
                }
            // No break;

            case 'copy':
            case 'delete':
            case 'show':
                if (!in_array(Input::get('id'), $root) || (Input::get('act') == 'delete' && !$this->User->hasAccess('delete', 'newp')))
                {
                    throw new Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' teaser manager section ID ' . Input::get('id') . '.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if (Input::get('act') == 'deleteAll' && !$this->User->hasAccess('delete', 'schwarmalarmp'))
                {
                    $session['CURRENT']['IDS'] = array();
                }
                else
                {
                    $session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (strlen(Input::get('act')))
                {
                    throw new Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' teaser manager section.');
                }
                break;
        }
    }

    public function setFirstGeoLatLon(DataContainer $dc)
    {

        if((int) $dc->activeRecord->lat == 0 || (int) $dc->activeRecord->lon == 0)
        {
            $this->setGeoLatLon($dc);
        }
    }

    public function setNewGeoLatLon($varValue, DataContainer $dc)
    {

        if((int) $varValue == 1)
        {
            $this->setGeoLatLon($dc);
        }
        return '';
    }

    /**
     * set the geolocation if its empty
     * @param \DataContainer
     */
    public function setGeoLatLon(DataContainer $dc)
    {

        $addressStr = urlencode($dc->activeRecord->street.' '.$dc->activeRecord->country);
//        print_r(strrpos($dc->activeRecord->street, " "));
        $searchData = [
            'plz' => $dc->activeRecord->zipcode,
            'address' => $addressStr,
            'string' => $dc->activeRecord->country.', '.substr($dc->activeRecord->street,0,strrpos($dc->activeRecord->street, " "))
        ];
        $geo = new \Srhinow\SchwarmalarmBundle\Libs\OsmGeoData();

        $json = $geo->getGeoData($searchData);

        if(is_object($json[0]))
        {
            $set = [
                'setnewgeo' => "",
                'lat' => $json[0]->lat,
                'lon' => $json[0]->lon
            ];

            Database::getInstance()->prepare('UPDATE `tl_schwarmalarm_beekeeper` %s WHERE id=?')
                ->set($set)
                ->limit(1)
                ->execute($dc->id);
        }

    }

    /**
     * set all geolocation if its empty
     * @param \DataContainer
     */
    public function setAllGeoLatLon(DataContainer $dc)
    {
        $resObj = $this->Database->prepare('SELECT * FROM `tl_schwarmalarm_beekeeper` WHERE lat=0 OR lon=0')->execute();

        if($resObj->numRows > 1)
        {
            $geo = new \Srhinow\SchwarmalarmBundle\Libs\OsmGeoData();

            while($resObj->next())
            {
                $addressStr = urlencode($dc->activeRecord->street.', '.$dc->activeRecord->country);
                $searchData = [
                    'plz' => $dc->activeRecord->zipcode,
                    'address' => $addressStr,
                    'string' => $dc->activeRecord->zipcode.' '.$dc->activeRecord->country.' '.$dc->activeRecord->street
                ];

                $json = $geo->getGeoData($searchData);

                if(is_object($json[0]))
                {
                    $set = [
                        'setnewgeo' => "",
                        'lat' => $json[0]->lat,
                        'lon' => $json[0]->lon
                    ];

                    $this->Database->prepare('UPDATE `tl_schwarmalarm_beekeeper` %s WHERE id=?')->set($set)->execute($resObj->id);
                }
            }
        }
    }


    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (\strlen(Input::get('tid')))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_schwarmalarm_beekeeper::published', 'alexf'))
        {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published'])
        {
            $icon = 'invisible.svg';
        }
        

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     *
     * @throws Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }

        // Trigger the onload_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['config']['onload_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['config']['onload_callback'] as $callback)
            {
                if (\is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (\is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        // Check the field access
        if (!$this->User->hasAccess('tl_schwarmalarm_beekeeper::published', 'alexf'))
        {
            throw new Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to publish/unpublish article ID "' . $intId . '".');
        }

        // Set the current record
        if ($dc)
        {
            $objRow = $this->Database->prepare("SELECT * FROM tl_schwarmalarm_beekeeper WHERE id=?")
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows)
            {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new Versions('tl_schwarmalarm_beekeeper', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['fields']['published']['save_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['fields']['published']['save_callback'] as $callback)
            {
                if (\is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                }
                elseif (\is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        $this->Database->prepare("UPDATE tl_schwarmalarm_beekeeper SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        if ($dc)
        {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['config']['onsubmit_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_schwarmalarm_beekeeper']['config']['onsubmit_callback'] as $callback)
            {
                if (\is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (\is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();
    }

    /**
     * Auto-generate an alias if it has not been set yet
     * @param mixed
     * @param DataContainer
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = standardize(\StringUtil::restoreBasicEntities($dc->activeRecord->title));
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_schwarmalarm_beekeeper WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);

        // Check whether the page alias exists
        if ($objAlias->numRows > 1)
        {
            if (!$autoAlias)
            {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

 
}
