<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Models;

use Contao\Model;

class SchwarmalarmPropertiesModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_schwarmalarm_properties';
    
}
