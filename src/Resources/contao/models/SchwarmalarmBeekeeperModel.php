<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Models;

use Contao\Model;

class SchwarmalarmBeekeeperModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_schwarmalarm_beekeeper';

	/**
     * Find beekeepers with specific radius around the geodata
     *
     * @param integer $intLimit     limit items
     * @param integer $intOffset     pagination offset
     * @param array $geodata     lat and lon paramer
     * @param integer $distance
     * @param array   $arrIds find from multiple ids
     * @param array   $arrOptions An optional options array
     *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findBeekeepers($intLimit=0, $intOffset=0, array $geodata=array(), $distance=1, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;
        $arrColumns[] = "$t.published = 1";

		if(strlen($geodata['lat']) && strlen($geodata['lon']) && (int) $distance > 0)
		{
			$arrColumns[] = "ACOS( SIN(RADIANS($t.lat)) * SIN(RADIANS(".$geodata['lat'].")) + COS(RADIANS($t.lat)) * COS(RADIANS(".$geodata['lat'].")) * COS(RADIANS($t.lon) - RADIANS(".$geodata['lon']."))) * 6380 <= ".$distance;
			// $arrOptions['order'] = "$t.distance DESC";
		}

		if(is_array($arrIds) && count($arrIds) > 0)
		{
			$arrColumns[] = "$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")";
		}

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.zipcode DESC";
		}
		
		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;


		return static::findBy($arrColumns, null, $arrOptions);
	}
	/**
	 * Count beekeepers with specific radius around the geodata
	 *
	 * @param array $geodata    lat and lon data
	 * @param integer $distance     the distance-cicle of poi
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function countBeekeepersEntries(array $geodata=array(), $distance=1, array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;
        $arrColumns[] = "$t.published = 1";

		if(strlen($geodata['lat']) && strlen($geodata['lon']) && (int) $distance > 0)
		{
			$arrColumns[] ="ACOS( SIN(RADIANS($t.lat)) * SIN(RADIANS(".$geodata['lat'].")) + COS(RADIANS($t.lat)) * COS(RADIANS(".$geodata['lat'].")) * COS(RADIANS($t.lon) - RADIANS(".$geodata['lon']."))) * 6380 <= ".$distance;
		}

		return static::countBy($arrColumns, null, $arrOptions);
	}
}
