<?php
/**
 * TL_ROOT/vendor/srhinow/schwarmalarm-bundle/src/Recources/contao/modules/languages/de/tl_schwarmalarm_beekeeper.php
 *
 * Contao extension: schwarmalarm-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/** Legends */
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['address_legend'] = "Adressdaten";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['geo_legend'] = "Geo Einstellungen";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['extend_legend'] = "weitere Einstellungen";

/** Fields */
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['name']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['phone']['0'] = "Telefonnummer";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['phone']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcode']['0'] = "PLZ";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcode']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcity']['0'] = "PLZ und Stadt";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['zipcity']['1'] = "12345 Berlin";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['country']['0'] = "Stadt";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['country']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['street']['0'] = "Strasse";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['street']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['additional']['0'] = "weitere Angaben";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['additional']['1'] = "";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lat']['0'] = "Latitude";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lat']['1'] = "Breitengrad";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lon']['0'] = "longitude";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['lon']['1'] = "Längengrad";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['key']['0'] = "Key";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['key']['1'] = "zum Eintrag eindeutig generierter Schlüssel";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['active']['0'] = "auf Website anzeigen";

/** Actions */
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['new']['0'] = "Neuer Eintrag";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['new']['1'] = "Neuen Eintrag erstellen.";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['show']['0'] = "Einzelheiten zum Eintrag";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['show']['1'] = "Einzelheiten zum Eintrag mit der ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['editheader']['0'] = "Eintrag bearbeiten";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['editheader']['1'] = "Eintrag ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['copy']['0'] = "Eintrag kopieren";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['copy']['1'] = "Eintrag ID %s kopieren";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['cut']['0'] = "Eintrag verschieben";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['cut']['1'] = "Eintrag ID %s verschieben";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['delete']['0'] = "Eintrag löschen";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['delete']['1'] = "Eintrag ID %s löschen";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['copyChildren']['0'] = "Eintrag mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['copyChildren']['1'] = "Eintrag ID %s mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['theme_section_article']['0'] = "Eintrag-Artikel";
$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['theme_section_article']['1'] = "Eintrag-Artikel bearbeiten";

/** Message */
//$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['success_categories_imported'] = 'Es wurden alle verfügbaren Kategorien geholt';
//$GLOBALS['TL_LANG']['tl_schwarmalarm_beekeeper']['error_no_categories_imported'] = 'Es konnten keine Kategorien geholt werden.';