<?php

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    SchwarmalarmBundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/srhinow/schwarmalarm-bundle
 *
 */

namespace Srhinow\SchwarmalarmBundle;


use Srhinow\SchwarmalarmBundle\DependencyInjection\SrhinowSchwarmalarmExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao schwarmalarm-bundle.
 *
 * @author Sven Rhinow
 */
class SrhinowSchwarmalarmBundle extends Bundle
{
    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new SrhinowSchwarmalarmExtension();
    }
}
