<?php
/**
 * Created by schwarmalarm-bundle.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 2018
 */

/**
 * -------------------------------------------------------------------------
 * MODULE CONSTANTS
 * -------------------------------------------------------------------------
 */
$GLOBALS['SCHWARMALARM']['SETUP']['ID'] = 1;
$GLOBALS['SCHWARMALARM']['PUBLIC_PATH'] = '/bundles/srhinowschwarmalarm';
$GLOBALS['SCHWARMALARM']['SESSION_KEY'] = 'beekeeperfilter';
$GLOBALS['BN']['BN_IMAGE_PATH'] = 'files/Bibliotheken';


/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */

$SchwarmalarmBe = array
(
    'schwarmalarm' => array
    (
        'tl_schwarmalarm_beekeeper' => array
        (
            'tables' => array('tl_schwarmalarm_beekeeper'),
            'icon'  => $GLOBALS['SCHWARMALARM']['PUBLIC_PATH'].'/icons/entity.png',
            'csvLibraryExport' => array('beCSVExport', 'csvLibraryExport')
        ),
        'tl_schwarmalarm_properties' => array
        (
            'callback'	=> 'Srhinow\SchwarmalarmBundle\Modules\ModuleSchwarmalarmSetup',
            'tables' => array('tl_schwarmalarm_properties'),
            'icon'		=> $GLOBALS['SCHWARMALARM']['PUBLIC_PATH'].'/icons/process.png',
        ),
        'tl_zip_coordinates' => array
        (
            'tables' => array('tl_zip_coordinates'),
        )
    )
);
array_insert($GLOBALS['BE_MOD'], 1, $SchwarmalarmBe);

/**
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
    'schwarmalarm' => array
    (
        'beekeeper_search_form'    => 'Srhinow\SchwarmalarmBundle\Modules\ModuleBeekeeperSearchForm',
        'beekeeper_search_list'    => 'Srhinow\SchwarmalarmBundle\Modules\ModuleBeekeeperSearchList',
        'beekeeper_search_map'    => 'Srhinow\SchwarmalarmBundle\Modules\ModuleBeekeeperSearchMap',
        'beekeeper_edit_entry'    => 'Srhinow\SchwarmalarmBundle\Modules\ModuleBeekeeperEditEntry',
        'beekeeper_verify_link'    => 'Srhinow\SchwarmalarmBundle\Modules\ModuleVerifyOptOutLink',
    )
));
/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'schwarmalarms';
$GLOBALS['TL_PERMISSIONS'][] = 'schwarmalarmp';

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_schwarmalarm_beekeeper'] = \Srhinow\SchwarmalarmBundle\Models\SchwarmalarmBeekeeperModel::class;
$GLOBALS['TL_MODELS']['tl_schwarmalarm_properties'] = \Srhinow\SchwarmalarmBundle\Models\SchwarmalarmPropertiesModel::class;


/**
 * Hooks
 */
//$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('srhinow_contaoblank.listener.insert_tags', 'onReplaceInsertTags');

