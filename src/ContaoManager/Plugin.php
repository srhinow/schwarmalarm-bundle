<?php

/**
 * @copyright  Sven Rhinow 2017 <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ContaoBlankBundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/srhinow/contao-blank-bundle
 *
 */

namespace Srhinow\SchwarmalarmBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

use Srhinow\SchwarmalarmBundle\SrhinowSchwarmalarmBundle;

/**
 * Plugin for the Contao Manager.
 *
 * @author Sven Rhinow
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowSchwarmalarmBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
