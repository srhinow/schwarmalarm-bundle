<?php

/*
 * This file is part of schwarmalarm-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\SchwarmalarmBundle\Modules;


use Contao\BackendTemplate;
use Contao\Controller;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Pagination;
use mysql_xdevapi\Session;
use Srhinow\SchwarmalarmBundle\Models\SchwarmalarmBeekeeperModel;


/**
 * Class ModuleBeekeeperSearchList
 */
class ModuleBeekeeperSearchList extends ModuleSchwarmalarm
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_beekeeper_search_list';

    /**
     * @var bool
     */
	public $showDistance = true;

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### IMKER-SUCH-LISTE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Set the item from the auto_item parameter
		if (!isset($_GET['s']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('s', Input::get('auto_item'));
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
    {
        $offset = intval($this->skipFirst);
        $limit = null;
        $this->Template->beekeepers = array();

        $swarmTime = $this->isSwarmTime();

        // Maximum number of items
        if ($this->numberOfItems > 0) {
            $limit = $this->numberOfItems;
        }

        $session = \Contao\Session::getInstance();
        $session = $session->get($GLOBALS['SCHWARMALARM']['SESSION_KEY']) ?: array();

        $this->showDistance = strlen($session['plzcity'])? true : false;
        
        if( (int)$session['distance'] > 0 && $swarmTime) {

            if (strlen($session['geo_lat']) > 0 && strlen($session['geo_lon']) > 0) {
                $geodata = array
                (
                    'lat' => $session['geo_lat'],
                    'lon' => $session['geo_lon']
                );
            } else {
                $geodata = $this->getGeoDataFromCurrentPosition();
            }

            // Get the total number of items
            $intTotal = SchwarmalarmBeekeeperModel::countBeekeepersEntries($geodata, $session['distance']);

            // Filter anwenden um die Gesamtanzahl zuermitteln
            if ($intTotal > 0) {
                $filterLibsObj = SchwarmalarmBeekeeperModel::findBeekeepers($intTotal, 0, $geodata, $session['distance']);

                $counter = 0;
                $idArr = array();

                while ($filterLibsObj->next()) {
                    //wenn alle Filter stimmen -> Werte setzen
                    $idArr[] = $filterLibsObj->id;
                    $counter++;
                }
                if ((int)$intTotal > $counter) $intTotal = $counter;
            }

            if ((int)$intTotal < 1) {
                $this->Template = new FrontendTemplate('mod_beekeeper_entries_empty');
                $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyBnList'];
                return;
            }

            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage)) {


                // Adjust the overall limit
                if (isset($limit)) {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = \Input::get($id) ?: 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                    global $objPage;

                    $objPage->noSearch = 1;
                    $objPage->cache = 0;

                    $objTarget = PageModel::findByPk($objPage->id);
                    if ($objTarget !== null) {
                        $reloadUrl = ampersand(\Controller::generateFrontendUrl($objTarget->row()));
                    }

                    $this->redirect($reloadUrl);
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset += (max($page, 1) - 1) * $this->perPage;
                $skip = intval($this->skipFirst);

                // Overall limit
                if ($offset + $limit > $total + $skip) {
                    $limit = $total + $skip - $offset;
                }

                // Add the pagination menu
                $objPaginationTemplate = new FrontendTemplate('pagination_bs');
                $objPagination = new Pagination($total, $this->perPage, $GLOBALS['TL_CONFIG']['maxPaginationLinks'], $id, $objPaginationTemplate);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }


            // Get the items
            if (isset($limit)) {

                $BeekeeperObj = SchwarmalarmBeekeeperModel::findBeekeepers($limit, $offset, $geodata, $session['distance'], $idArr);
            } else {

                $BeekeeperObj = SchwarmalarmBeekeeperModel::findBeekeepers(0, $offset, $geodata, $session['distance'], $idArr);
            }

            // No items found
            if ($BeekeeperObj === null) {
                $this->Template = new FrontendTemplate('mod_newsarchive_empty');
                $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];
            } else {

                $this->Template->beekeepers = $this->parseBeekeepers($BeekeeperObj,$this->showDistance);
            }

    //        $GLOBALS['TL_JAVASCRIPT'][] = PUBLIC_SRC_PATH.'/js/bn_fe.js';

            $this->Template->filterActive = Input::get('s') ? true : false;
            $this->Template->totalItems = $intTotal;
        }

        $this->Template->swarmTime = $swarmTime;
        $this->Template->showDistance = $this->showDistance;
	}
}
